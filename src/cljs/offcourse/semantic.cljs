(ns offcourse.semantic
(:require [reagent.core :as r]
          [cljsjs.semantic-ui-react]))

(def semantic-ui js/semanticUIReact)

(def Container   (r/adapt-react-class (goog.object/get semantic-ui "Container")))

(def Header        (r/adapt-react-class (goog.object/get semantic-ui "Header")))
(def HeaderContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Header" "Content")))

(def Icon        (r/adapt-react-class (goog.object/get semantic-ui "Icon")))

(def Button      (r/adapt-react-class (goog.object/get semantic-ui "Button")))

(def JSSegment   (goog.object/get semantic-ui "Segment"))
(def Segment     (r/adapt-react-class (goog.object/get semantic-ui "Segment")))

(def JSMenu         (goog.object/get semantic-ui "Menu"))
(def Menu           (r/adapt-react-class (goog.object/get semantic-ui "Menu")))

(def JSCard          (goog.object/get semantic-ui "Card"))
(def Card            (r/adapt-react-class (goog.object/get semantic-ui "Card")))
(def CardContent     (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Card" "Content")))
(def CardHeader      (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Card" "Header")))

(def JSSidebar       (goog.object/get semantic-ui "Sidebar"))
(def SemanticSidebar (r/adapt-react-class JSSidebar))
(def SidebarPushable (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Sidebar" "Pushable")))
(def SidebarPusher   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Sidebar" "Pusher")))

(def JSAccordion      (goog.object/get semantic-ui "Accordion"))
(def Accordion        (r/adapt-react-class (goog.object/get semantic-ui "Accordion")))
(def AccordionTitle   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Accordion" "Title")))
(def AccordionContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Accordion" "Content")))

(def JSTable          (goog.object/get semantic-ui "Table"))
(def Table            (r/adapt-react-class (goog.object/get semantic-ui "Table")))
(def TableHeader      (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Header")))
(def TableBody        (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Body")))
(def TableFooter      (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Footer")))
(def TableRow         (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Row")))
(def TableHeaderCell  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "HeaderCell")))
(def TableCell        (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Cell")))
