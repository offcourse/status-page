(ns offcourse.core
  (:require [reagent.core :as r]
            [ajax.core :refer [GET]]
            [offcourse.semantic :refer [Container Accordion Header HeaderContent Icon
                                        AccordionTitle AccordionContent Segment
                                        Table TableHeader TableHeaderCell TableBody TableRow TableCell]]
            [offcourse.layout :refer [Layout]]
            [offcourse.sidebar :refer [Sidebar]]
            [offcourse.course-card :refer [CourseCard]]))

(defn service-row [[title info]]
  ^{:key title} [TableRow
                 [TableCell title]
                 (if (string? info)
                   ^{:key info} [TableCell info]
                   (map (fn [val] ^{:key val}[TableCell val])(vals info)))])


(defn services-table [index [service-name service-data]]
  (let [orientation (if (= index 0) :top true)]
    ^{:key service-name}[Table {:celled true
                                :compact true
                                :attached orientation}
                         [TableHeader
                          [TableRow {:textAlign :center}
                           [TableHeaderCell {:colSpan 3} service-name]]]
                         [TableBody (map service-row service-data)]]))

(defn environment-section [title data]
  [:div [AccordionTitle  [Header title]
         [AccordionContent
          (map-indexed services-table data)]]])

(defonce appstate (r/atom {:sidebar-visible? false}))

(defn App [appstate]
  [Layout appstate
   [Sidebar appstate]
   [Container
    [Segment {:basic true}
    [Accordion {:default-active-index 0
                :exclusive false
                :fluid true}
     [AccordionTitle
      [Header [Icon {:name :globe}] [HeaderContent "Global"]]]
     [AccordionContent (map-indexed services-table (:global @appstate))]
     [AccordionTitle
      [Header [Icon {:name :pencil}] [HeaderContent "Development"]]]
     [AccordionContent (map-indexed services-table (:development @appstate))]]]]])

(GET "https://s3.amazonaws.com/offcourse-services-info-global/global.json"
     {:handler #(swap! appstate assoc-in [:global] %)
      :error-handler #(.log js/console %)
      :response-format :json
      :keywords? true})

(GET "https://s3.amazonaws.com/offcourse-services-info-global/development.json"
     {:handler #(swap! appstate assoc-in [:development] %)
      :error-handler #(.log js/console %)
      :response-format :json
      :keywords? true})

(defn render-simple []
  (r/render-component [App appstate]
                      (. js/document (querySelector "#container"))))
